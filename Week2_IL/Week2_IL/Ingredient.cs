﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2_IL
{
    class Ingredient
    {
        public string Name { get; set; }

        public Ingredient(string name)
        {
            this.Name = name;
        }

        public virtual void MixIn()
        {
            Console.WriteLine($"Mixing {Name} into the bowl...");
        }
    }

    class Solid : Ingredient
    {
        public int Grams {get; set;}
        public Solid(string name, int grams) : base(name)
        {
            this.Grams = grams;
        }

        // Overriding the base's MixIn method
        public override void MixIn()
        {
            Console.WriteLine($"Mixing {Grams}g of {Name}...");
        }
    }

    class Liquid : Ingredient
    {
        public int Milliliters { get; set; }

        public Liquid(string name, int liquid) : base(name)
        {
            this.Milliliters = liquid;
        }

        public override void MixIn()
        {
            Console.WriteLine($"Pouring {Milliliters}ml of {Name}...");
            Console.WriteLine($"Mixing until the {Name} is fully combined...");
        }
    }
    
    class Egg : Solid
    {
        // Large egg is ~60 grams
        public Egg() : base("Egg", 60)
        {

        }

        public override void MixIn()
        {
            Console.WriteLine($"Cracking Egg...");
            Console.WriteLine("Mixing until smooth.");
        }
    }
}
