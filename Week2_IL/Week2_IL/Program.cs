﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2_IL
{
    class Program
    {
        // Bailey Seymour
        // DVP1
        // 03-11-2018
        static void Main(string[] args)
        {
            Console.WriteLine("-----------");
            Console.WriteLine("Properties Experiments");
            Console.WriteLine("-----------");
            PropertiesExample();
            


            Console.WriteLine("-----------");
            Console.WriteLine("Polymorphism Experiments");
            Console.WriteLine("-----------");
            PolymorphismExample();
            
        }

        public static void PropertiesExample()
        {
            // Properties Example
            User userA = new User("bailey", "passwd");
            Console.WriteLine("Username: " + userA.UserName);

            // This is a test to see if the setter blocks empty usernames:
            userA.UserName = "   ";
            // it should still be `bailey`
            Console.WriteLine("Username: " + userA.UserName);

            // set the UserName property to a new value
            userA.UserName = "mac43";
            Console.WriteLine("Username: " + userA.UserName);

            // Private password property is working:
            // Error cannot access due to the protection level
            //  Console.WriteLine("Password is: "+userA.Password);

            // Testing that the HighScore property cannot be set outside of our class:
            Console.WriteLine($"{userA.UserName}'s HighScore is "+ userA.HighScore+"!");
            userA.Play();
            Console.WriteLine($"{userA.UserName}'s HighScore is " + userA.HighScore + "!");
            // This should not be allowed:
            // userA.HighScore = 10000000; // Does not work since HighScore's set is marked as private.

            Console.WriteLine($"{userA.UserName}'s HighScore is " + userA.HighScore + "!");
            userA.Play();

            Console.WriteLine($"{userA.UserName}'s HighScore is " + userA.HighScore + "!");
        }

        public static void PolymorphismExample()
        {
            /* A simple recipe example used to experiment with with polymorphism...
             * The MixIn() method prints different output based on the kind of object each Ingredient is.
             */
            List<Ingredient> ingredients = new List<Ingredient>();
            Liquid water = new Liquid("Water", 250);
            Solid sugar = new Solid("Sugar", 50);

            // Lower the amount of sugar
            sugar.Grams = sugar.Grams / 2;

            Egg egg = new Egg();
            Egg egg2 = new Egg();

            ingredients.Add(water);
            ingredients.Add(sugar);
            ingredients.Add(egg);
            ingredients.Add(egg2);

            foreach (Ingredient ingredient in ingredients)
            {
                // Check to see what type ingredient really is: 
                Console.WriteLine("*** " + ingredient.GetType());
                // Add each ingredient in our recipe to the mixing bowl:
                ingredient.MixIn();
                Console.Write("\r\n");
            }
            
        }
    }
}
