﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2_IL
{
    class User
    {
        // UserName property
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    _UserName = value;
            }
        }

        private string _UserName;

        private string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }

        private string _Password;

        public bool IsLoggedIn { get; set; } = false;

        public int HighScore { get; private set; } = 0;

        public User(string userName, string password)
        {
            this.UserName = userName;
            this.Password = password;
        }

        public void Play()
        {
            this.IsLoggedIn = true;
            // Increase the score
            this.HighScore += 100;
        }
    }
}
