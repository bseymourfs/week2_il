## Week 2 IL Notes

### C# Properties

Properties in C# are nested in brackets with the keywords `get` and `set` like so:

```csharp

public <datatype> <PropertyName>
{
get {
    // Code goes here that returns a value (typically a private member variable)
}
set {
 // Code goes here that changes a private member variable
}
}
// Declare a private member variable to hold the data for our property.
private <datatype> _<PropertyName>;
```

The `value` keyword can be used like a variable to manipulate or validate the incoming data inside of the set property block.

Instead of methods like `GetPropertyName()` and `SetPropertyName()`, C# properties act like member variables.

Microsoft includes a shorthand syntax for creating simple properties:

```csharp
// For example a simple property:
public string FirstName {get; set;}

// Or a property with a default value
public bool IsVisible {get; set;} = true;

// Readonly property — Simply by omitting the `set` keyword.
public bool IsVisible {get;} = true;

// Public getter and private setter:
public string FirstName {get; private set;}
```



##### When should properties be used instead of methods? 

Properties should be used when dealing with data and methods should be used when performing an action.



*Resources:*

[docs.microsoft.com](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties)

[Essential C# 6.0](http://ce.safaribooksonline.com/book/programming/csharp/9780134176147/5dot-classes/ch05lev1sec6_html?uicode=fullsail)

------

### C# Polymorphism

- **Subclasses can either inherit parent methods or override them.**
- You can't have polymorphism without inheritance.
- However, you can have inheritance without polymorphism.

`virtual` methods can be overwritten using the `override` keyword from a subclass.



If a Shape class has an Area method that returns the area.

`Shape.Area()`

Subclasses like Rectangle and Circle can override that method with their own implementation.

`Rectangle.Area()`

Later on the program you could have an `List<Shape>`:

```csharp
List<Shape> shapes = new List<Shape>{new Rectange(10, 10), new Circle(4)};
// You can then loop through each shape like so:
for (int i=0; i < shapes.Count; i++) {
    Shape myShape = shapes[i];
    Console.WriteLine("The area of "+myShape.Description()+" is: "+myShape.Area());
}
```

So as you can see, even though we are treating the Rectangles and Circles as a `Shape`, it still calls the method from the subclasses instead of the base class which is `Shape`.



*Resources:*

[Introduction to Polymorphism in C# (Video)](https://youtu.be/18D8R3hwYW8)

[Polymorphism in C#](http://csharp-station.com/Tutorial/CSharp/Lesson09)

[docs.microsoft.com — Polymorphism](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/polymorphism)